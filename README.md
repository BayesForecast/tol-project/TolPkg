# TolPkg

This is a TOL package implementing functionalities to help the user to
build, package and distribute any TOL package.

Originally this code was distributed inside `TolPackage` from
`TolCore` but as this is not needed to boostrap TOL it is best moved
into a specific package in order to evolve without recompiling TOL.
